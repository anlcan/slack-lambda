import json
import unittest
from os import environ

import lambda_function
import requests


class TestStringMethods(unittest.TestCase):

    def test_slack(self):
        environ["SLACK_WEBHOOK"] = "https://hooks.slack.com/services/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"
        with open("gitlab_merge_request.json") as payload:
            lambda_function.lambda_handler(json.load(payload), None)

    def test_api(self):
        api_url = "https://7e223m5gle.execute-api.eu-central-1.amazonaws.com/production"
        with open("gitlab_merge_request.json") as payload:
            json_data= json.load(payload)
            print(json.dumps(json_data))
            response = requests.post(api_url, json=json_data)
            print(response)
