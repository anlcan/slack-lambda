import json
import logging
from os import environ

from botocore.vendored import requests

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# gitlab->slack mapping
USERS = {"anlcan": "anil",
         "Diego_Amicabile": "U9GV6SX4M",
         "hespoz.infinitec": 'U97HZ0S8J',
         "sergey.petrochenko": "U4PED9554"
         }


def respond(res, err=None):
    return {
        'statusCode': '400' if err else '200',
        'body': err.message if err else json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
        },
    }


def lambda_handler(event, context):
    """
    receives webhook from gitlab and forwards it to slack channel
    """
    object_kind = event["object_kind"]
    if object_kind == "merge_request":
        handle_merge_request(event)
    else:
        post_slack("%s on `%s`" % (object_kind, event["project"]["name"]))
    return respond({"status": "OK"})


def post_slack(text):
    SLACK_WEBHOOK = environ['SLACK_WEBHOOK']
    requests.post(SLACK_WEBHOOK, json={"text": text})


def resolve(name):
    if USERS[name]:
        return "<@%s>" % USERS[name]
    else:
        return name


def handle_merge_request(event):
    assignee = resolve(event["object_attributes"]["assignee"]["name"])
    author = resolve(event["object_attributes"]["last_commit"]["author"]["name"])

    text = "`%s` to %s from %s\n%s" \
           % (event['project']['name'],
              assignee,
              author,
              event["project"]["web_url"]
              )
    post_slack(text)
