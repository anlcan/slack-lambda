#!/usr/bin/env bash

LAMBDA_NAME="slack-lambda"   # Replace it with the name of aws lambda function you want.
S3_BUCKET="gitlab-lambda"
LAMBDA_RUNTIME="python3.6"
LAMBDA_ARN="arn:aws:iam::703225442232:role/lambda_basic_execution" # ARN associated with this lambda function.
LAMBDA_HANDLER="lambda_function.lambda_handler"  # This is default lambda handler.


zip -r code.zip lambda_function.py # Archive the code repository.
aws s3 --profile finleap cp code.zip s3://${S3_BUCKET}/code.zip # Upload archive into s3.
aws lambda --profile finleap update-function-code --function-name ${LAMBDA_NAME}  --zip-file fileb://code.zip
#aws lambda --profile finleap update-function --function-name ${LAMBDA_NAME} --runtime ${LAMBDA_RUNTIME} \
#    --role ${LAMBDA_ARN} --handler ${LAMBDA_HANDLER} --code S3Bucket=${S3_BUCKET},S3Key=code.zip --memory-size 1024

